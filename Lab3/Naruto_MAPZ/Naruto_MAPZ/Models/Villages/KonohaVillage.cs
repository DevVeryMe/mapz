﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Naruto_MAPZ.Models.Villages
{
    class KonohaVillage : AbstractVillage
    {
        public KonohaVillage(string style) : base(style)
        {
            _style += "Fire Style";
        }
    }
}
