﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Naruto_MAPZ.Models.Villages
{
    class SunagakureVillage : AbstractVillage
    {
        public SunagakureVillage(string style) : base(style)
        {
            _style += "Sand Style";
        }
    }
}
