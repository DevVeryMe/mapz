﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Naruto_MAPZ.Models.Ninjas
{
    //[1]CONCRETE PROTOTYPE
    //[2]CONCRETE FLYWEIGHT
    public class HealingNinja : NinjaPrototype //[1]CONCRETE PROTOTYPE [2]CONCRETE FLYWEIGHT
    {
        public HealingNinja(string name, NinjaRank rank) : base(name, rank)
        {

        }
        public override NinjaPrototype Clone()
        {
            return new HealingNinja(_name, _rank);
        }
        public override int Hit()
        {
            int damage = _baseDamege + (int)(_level * 0.5);
            return damage;
        }
    }
}
