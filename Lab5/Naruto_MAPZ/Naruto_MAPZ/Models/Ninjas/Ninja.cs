﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Naruto_MAPZ.Models.Ninjas
{
    //[1]PROTOTYPE
    //[2]FLYWEIGHT
    public abstract class NinjaPrototype //[1]PROTOTYPE [2]FLYWEIGHT
    {
        protected string _name;
        protected NinjaRank _rank;
        protected readonly int _baseDamege;
        protected int _level;
        protected int _experience;

        public NinjaPrototype(string name, NinjaRank rank)
        {
            _name = name;
            _rank = rank;
            _level = 1;
            _experience = 0;

            switch(_rank)
            {
                case NinjaRank.BEGINER:
                    _baseDamege = 5;
                    break;
                case NinjaRank.GENIN:
                    _baseDamege = 10;
                    break;
                case NinjaRank.CHUNIN:
                    _baseDamege = 15;
                    break;
                case NinjaRank.JONIN:
                    _baseDamege = 25;
                    break;
                case NinjaRank.SANIN:
                    _baseDamege = 40;
                    break;
                case NinjaRank.LEGEND:
                    _baseDamege = 55;
                    break;
                default:
                    break;
            }
        }
        public string Name
        {
            get
            {
                return _name;
            }
        }
        public NinjaRank Rank
        {
            get
            {
                return _rank;
            }
        }
        public abstract NinjaPrototype Clone();
        public abstract int Hit();
    }

    public enum NinjaRank
    {
        BEGINER,
        GENIN,
        CHUNIN,
        JONIN,
        SANIN,
        LEGEND
    }
}
