﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Naruto_MAPZ.Interfaces;

namespace Naruto_MAPZ.Models.Missions
{
    //[1]COMMAND
    public class CurrentMissionCommand : ICommand //[1] CONCRETE COMMAND
    {
        public void Execute()
        {
            User.Instance().AcceptMission();
        }

        public void Undo()
        {
            User.Instance().DenyMission();
        }
    }
}
