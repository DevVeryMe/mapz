﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Naruto_MAPZ.Models.Enemies
{
    //[1] STRATEGY
    public class Enemy //[1] CONTEXT
    {
        private EnemyRank _rank;
        private readonly int _baseDamege;
        private readonly int _level;

        public IHitable Hitable { get; private set; }

        public Enemy(string name, EnemyRank rank, int level, IHitable hitable)
        {
            _rank = rank;
            _level = level;
            Hitable = hitable;

            switch (_rank)
            {
                case EnemyRank.SIMPLE:
                    _baseDamege = 5;
                    break;
                case EnemyRank.MEDIUM:
                    _baseDamege = 10;
                    break;
                case EnemyRank.HARD:
                    _baseDamege = 15;
                    break;
                case EnemyRank.NUKENIN:
                    _baseDamege = 25;
                    break;
                default:
                    break;
            }
        }

        public int Hit()
        {
            return Hitable.Hit(_baseDamege, _level);
        }
    }

    public enum EnemyRank
    {
        SIMPLE,
        MEDIUM,
        HARD,
        NUKENIN
    }
}
