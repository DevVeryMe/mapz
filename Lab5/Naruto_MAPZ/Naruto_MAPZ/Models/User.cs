﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Naruto_MAPZ.Models.Villages;
using Naruto_MAPZ.VillageFactory;
using Naruto_MAPZ.Models.Ninjas;
using Naruto_MAPZ.Memento;
using Naruto_MAPZ.Models.Observer;

namespace Naruto_MAPZ.Models
{
    //[1]SAFETY THREAD SINGLETON REALIZATION
    //[2]FACADE
    //[3]MEMENTO
    //[4]OBSERVER
    //[5]COMMAND
    public class User : IObservable //[3] MEMENTO ORIGINATOR && [4] OBSERVER OBSERVABLE && [5] COMMAND RECIEVER
    {
        private static User _instance; //[1]
        private static object _syncRoot;

        private string _nickName;
        private int _cash;
        private int _experience;
        private int _level;
        private AbstractVillage _village;
        private Bag _bag; //[2]FACADE SUBSYSTEM
        private NinjaBase _ninjaBase; //[2]FACADE SUBSYSTEM
        private List<MissionCustomer> _missionCustomers;
        private Queue<Mission> _missions;
        private Mission _currentMission;

        private User(string nickName, AbstractVillageFactory factory)
        {
            _nickName = nickName;
            _village = factory.CreateVillage();
        }

        public static User Instance(string nickName = "null", AbstractVillageFactory factory = null) //[1]
        {
            if (_instance == null)
            {
                lock (_syncRoot)
                {
                    if (_instance == null)
                    {
                        _instance = new User(nickName, factory);
                    }
                }
            }

            return _instance;
        }

        public int Cash
        {
            get
            {
                return _cash;
            }
        }

        public int Level 
        { 
            get
            {
                return _level;
            }
        }

        public void NewMissionAvailable(Mission mission)
        {
            _missions.Enqueue(mission);
        }

        public void AcceptMission() //[5] USED IN COMMAND
        {
            _currentMission = _missions.Dequeue();
            if (_ninjaBase.Ninjas.Count > 0)
            {
                _currentMission.Complete(_ninjaBase.Ninjas["Cap"]);
            }
        }

        public void DenyMission() //[5] USED IN COMMAND
        {
            _missions.Dequeue();
        }

        public void ViewBag() //[2]FACADE OPERATION
        {
            foreach (var item in _bag.Items)
            {
                Console.WriteLine(item.Key + (item.Value as Item).Description);
            }
        }

        public void ViewNinjaBase() //[2]FACADE OPERATION
        {
            foreach (var item in _ninjaBase.Ninjas)
            {
                Console.WriteLine(item.Key + item.Value.Name);
            }
        }

        public void HireNinja(string name, NinjaRank rank) //[2]FACADE OPERATION
        {
            _ninjaBase.AddNinja(new AttackingNinja(name, rank));
        }

        public void FireNinja(string name) //[2]FACADE OPERATION
        {
            _ninjaBase.RemoveNinja(name);
        }

        public UserMemento SaveState() //[3]MEMENTO SAVE STATE
        {
            UserMemento memento = new UserMemento();
            memento.Save(_cash, _level, _experience, _ninjaBase, _bag);
            return memento;
        }

        public void RestoreState() //[3] MEMENTO RESTORE STATE
        {
            UserMemento memento = new UserMemento();
            memento = UserMemento.LoadLast();

            this._cash = memento.Cash;
            this._level = memento.Level;
            this._experience = memento.Experience;
            this._ninjaBase = memento.NinjaBase;
            this._bag = memento.Bag;
        }

        public void AddObserver(IObserver o) //[4] ADD OBSERVER
        {
            if (o is MissionCustomer)
            {
                _missionCustomers.Add(o as MissionCustomer);
            }
        }

        public void RemoveObserver(IObserver o) //[4] REMOVE OBSERVER
        {
            if (o is MissionCustomer)
            {
                _missionCustomers.Remove(o as MissionCustomer);
            }
        }

        public void NotifyObservers() //[4] NOTIFY OBSERVERS
        {
            foreach (IObserver observer in _missionCustomers)
            {
                observer.Update(_currentMission);
            }
        }
    }
}
