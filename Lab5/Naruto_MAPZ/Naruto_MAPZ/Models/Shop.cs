﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Naruto_MAPZ.Models
{
    //[1]LAZY SINGLETON REALIZATION
    public class Shop
    {
        private static readonly Lazy<Shop> lazy = new Lazy<Shop>(() => new Shop()); //[1]

        private Shop()
        {

        }

        public static Shop Instance() //[1]
        {
            return lazy.Value;
        }
    }
}
