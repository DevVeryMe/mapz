﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Naruto_MAPZ.Interfaces;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Naruto_MAPZ.Models
{
    public class Item : IShopItem, IBagItem
    {
        private string _name;
        private string _description;
        private int _power;
        private int _price;
        private int _upgradeLevel;


        public string Name
        {
            get
            {
                return _name;
            }
        }
        public int Power
        {
            get
            {
                return _power;
            }
        }
        public string Description
        {
            get
            {
                return _description;
            }
        }

        public int ShopPrice
        {
            get
            {
                return _price;
            }
        }

        public int SellPrice
        {
            get
            {
                int sellPrice = _price - (int)Math.Round(_price * 0.7);
                return sellPrice;
            }
        }

        public int UpgradePrice
        {
            get
            {
                int upgradePrice = (int)Math.Round(_price * 0.15) * _upgradeLevel;
                return upgradePrice;
            }
        }

        public void Upgrade()
        {

        }
    }
}
