﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Naruto_MAPZ.Interfaces
{
    public interface IShopItem
    {
        int ShopPrice { get; }
    }
}
