﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Naruto_MAPZ.Interfaces;
using Naruto_MAPZ.Models.Missions;

namespace Naruto_MAPZ
{
    //[1]COMMAND

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window //[1] COMMAND INVOKER
    {
        private ICommand _command; //[1] COMMAND OBJECT
        public MainWindow()
        {
            InitializeComponent();
            SetCommand(new CurrentMissionCommand()); //[1] SETTING COMMAND
        }

        public void SetCommand(ICommand command) //[1] SET COMMAND METHOD
        {
            _command = command;
        }

        private void acceptMissionButton_Click(object sender, RoutedEventArgs e) //[1] COMMAND DO
        {
            _command.Execute();
        }

        private void denyMissionButton_Click(object sender, RoutedEventArgs e) //[1] COMMAND UNDO
        {
            _command.Undo();
        }
    }
}
