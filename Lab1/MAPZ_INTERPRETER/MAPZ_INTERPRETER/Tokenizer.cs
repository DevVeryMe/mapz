﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace MAPZ_INTERPRETER
{
    public sealed class Tokenizer
    {
        private string _inputCode;
        private int _index;
        private readonly Dictionary<TokenType, string> _tokens;
        private readonly Dictionary<TokenType, MatchCollection> _regExMatchCollection;

        public string InputCode
        {
            set
            {
                ResetParser();
                _inputCode = value;
                PrepareRegex();
            }
        }

        public Tokenizer(string input)
        {
            _inputCode = input;
            _index = 0;
            _tokens = new Dictionary<TokenType, string>();
            _regExMatchCollection = new Dictionary<TokenType, MatchCollection>();

            // KEY_WORDS
            _tokens.Add(TokenType.VAR_KEYWORD, @"\bvar\b");
            _tokens.Add(TokenType.OBJECT_KEYWORD, @"\bobject\b");

            // LITERALS
            _tokens.Add(TokenType.INT_LIT, @"\b(?![a-zA-Z_])\b[0-9][0-9]*\b(?![a-zA-Z_])\b");
            _tokens.Add(TokenType.STRING_LIT, "\".*?\"");

            // OPERATORS
            _tokens.Add(TokenType.DECREMENT, "\\-\\-");
            _tokens.Add(TokenType.EQUALS, "\\=\\=");
            _tokens.Add(TokenType.INCREMENT, "\\+\\+");
            _tokens.Add(TokenType.LESS_OR_EQUAL, "\\<\\=");
            _tokens.Add(TokenType.LESS_THAN, "\\<");
            _tokens.Add(TokenType.MORE_OR_EQUAL, "\\>\\=");
            _tokens.Add(TokenType.MORE_THAN, "\\>");
            _tokens.Add(TokenType.NOT_EQUALS, "\\!\\=");

            // OPERATIONS
            _tokens.Add(TokenType.ASSIGN_OP, "\\=");
            _tokens.Add(TokenType.SUBSTACT_OP, "\\-");
            _tokens.Add(TokenType.MULTIPLY_OP, "\\*");
            _tokens.Add(TokenType.DIVISION_OP, "\\/");
            _tokens.Add(TokenType.SUM_OP, "\\+");

            // EMBEDDED_FUNCTIONS
            _tokens.Add(TokenType.PRINT, @"\bPrint\b");
            _tokens.Add(TokenType.CLICK_ELEMENT, @"\bClickElement\b");
            _tokens.Add(TokenType.FILL_TEXT_FIELD, @"\bFillTextField\b");
            _tokens.Add(TokenType.LOAD_URL, @"\bLoadUrl\b");

            // BLOCKS
            _tokens.Add(TokenType.WHILE, @"\bwhile\b");
            _tokens.Add(TokenType.FOR, @"\bfor\b");
            _tokens.Add(TokenType.IF, @"\bif\b");
            _tokens.Add(TokenType.ELSE_IF, @"\belse_if\b");
            _tokens.Add(TokenType.ELSE, @"\belse\b");

            // IGNORABLE
            _tokens.Add(TokenType.NEW_LINE, "\\n");
            _tokens.Add(TokenType.COMMENT, "\\//");
            _tokens.Add(TokenType.WHITE_SPACE, "[ \\t]+");

            // DEFAULTS
            _tokens.Add(TokenType.DOT, "\\.");
            _tokens.Add(TokenType.COMMA, "\\,");
            _tokens.Add(TokenType.LEFT_PARAN, "\\(");
            _tokens.Add(TokenType.RIGHT_PARAN, "\\)");
            _tokens.Add(TokenType.LEFT_BRACE, "\\{");
            _tokens.Add(TokenType.RIGHT_BRACE, "\\}");
            _tokens.Add(TokenType.IDENT, @"\b(?!var |object |ClickElement |LoadUrl |FillTextField |Print |for |while |if |else_if |else )\b[a-zA-Z_][a-zA-Z0-9_]*");
            _tokens.Add(TokenType.SEMICOLON, "\\;");

            PrepareRegex();
        }

        private void PrepareRegex()
        {
            _regExMatchCollection.Clear();
            foreach (KeyValuePair<TokenType, string> pair in _tokens)
            {
                _regExMatchCollection.Add(pair.Key, Regex.Matches(_inputCode, pair.Value));
            }
        }
        public void ResetParser()
        {
            _index = 0;
            _inputCode = string.Empty;
            _regExMatchCollection.Clear();
        }
        public Token GetToken()
        {
            if (_index >= _inputCode.Length)
            {
                return new Token(string.Empty, TokenType.EOF);
            }

            foreach (KeyValuePair<TokenType, MatchCollection> pair in _regExMatchCollection)
            {
                foreach (Match match in pair.Value)
                {
                    if (match.Index == _index)
                    {
                        _index += match.Length;
                        return new Token(match.Value, pair.Key);
                    }

                    if (match.Index > _index)
                    {
                        break;
                    }
                }
            }
            _index++;
            return new Token(string.Empty, TokenType.UNDEFINED);
        }
    }
}
