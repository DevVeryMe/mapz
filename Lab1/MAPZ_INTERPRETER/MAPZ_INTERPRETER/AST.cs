﻿using System.Collections.Generic;

namespace MAPZ_INTERPRETER
{
    public class Expr { }
    public class Stmt { }

    public class Block : Stmt
    {
        public List<Stmt> Statements;
        public List<Variable> Variables;
        public List<CustomObject> Objects;

        public Block()
        {
            Statements = new List<Stmt>();
            Variables = new List<Variable>();
            Objects = new List<CustomObject>();
        }

        public void AddStmt(Stmt stmt)
        {
            Statements.Add(stmt);
        }

        public void AddVar(Variable variable)
        {
            Variables.Add(variable);
        }

        public void AddObj(CustomObject obj)
        {
            Objects.Add(obj);
        }
    }

    public class ObjectBlock : Expr
    {
        public Dictionary<string ,Variable> Fields;

        public ObjectBlock(Dictionary<string, Variable> fields)
        {
            Fields = fields;
        }
        public void AddVar(Variable variable)
        {
            Fields.Add(variable.Name, variable);
        }
    }

    public class ConditionBlock : Block
    {
        public Expr LeftExpr;
        public BooleanOperator Operator;
        public Expr RightExpr;

        public string GetOperator
        {
            get
            {
                string op = string.Empty;
                switch (Operator)
                {
                    case BooleanOperator.EQUALS:
                        op = "==";
                        break;
                    case BooleanOperator.LESS_OR_EQUAL:
                        op = "<=";
                        break;
                    case BooleanOperator.LESS_THAN:
                        op = "<";
                        break;
                    case BooleanOperator.MORE_OR_EQUAL:
                        op = ">=";
                        break;
                    case BooleanOperator.MORE_THAN:
                        op = ">";
                        break;
                    case BooleanOperator.NOT_EQUALS:
                        op = "!=";
                        break;
                    default:
                        break;
                }
                return op;
            }
        }
        public ConditionBlock() { }
        public ConditionBlock(Expr lexpr, BooleanOperator o, Expr rexpr)
        {
            LeftExpr = lexpr;
            Operator = o;
            RightExpr = rexpr;
        }
    }

    public class IfBlock : ConditionBlock
    {
        public IfBlock() { }
        public IfBlock(Expr lexpr, BooleanOperator o, Expr rexpr) : base(lexpr, o, rexpr) { }
    }

    public class ElseIfBlock : ConditionBlock
    {
        public ElseIfBlock() { }
        public ElseIfBlock(Expr lexpr, BooleanOperator o, Expr rexpr) : base(lexpr, o, rexpr) { }
    }

    public class ElseBlock : Block { }

    public class WhileBlock : ConditionBlock 
    {
        public WhileBlock() { }
        public WhileBlock(Expr lexpr, BooleanOperator o, Expr rexpr) : base(lexpr, o, rexpr) { }
    }

    public class Declaration : Stmt
    {
        public string Ident { get; private set; }

        public Declaration() { }
        public Declaration(string ident)
        {
            Ident = ident;
        }
    }

    public class Assign : Stmt
    {
        public string Ident { get; set; }
        public Expr Value { get; set; }

        public Assign() { }
        public Assign(string ident, Expr value)
        {
            Ident = ident;
            Value = value;
        }
    }

    public class Call : Stmt
    {
        public string Ident { get; private set; }
        public List<Expr> Args;

        public Call() { }
        public Call(string ident, List<Expr> args)
        {
            Ident = ident;
            Args = args;
        }
    }

    public class IntLiteral : Expr
    {
        public int Value;

        public IntLiteral() { }
        public IntLiteral(int v)
        {
            Value = v;
        }
    }

    public class StringLiteral : Expr
    {
        public string Value;

        public StringLiteral() { }
        public StringLiteral(string v)
        {
            Value = v;
        }
    }

    public class Ident : Expr
    {
        public string Value { get; private set; }

        public Ident() { }
        public Ident(string value)
        {
            Value = value;
        }
    }

    public class ObjectIdent : Expr
    {
        public string Name { get; private set; }
        public string Field { get; private set; }

        public ObjectIdent() { }
        public ObjectIdent(string name, string field)
        {
            Name = name;
            Field = field;
        }
    }

    public class MathExpr : Expr
    {
        public Expr LeftExpr;
        public MathOperation Operation;
        public Expr RightExpr;

        public MathExpr() { }
        public MathExpr(Expr lexpr, MathOperation o, Expr rexpr)
        {
            LeftExpr = lexpr;
            Operation = o;
            RightExpr = rexpr;
        }
    }

    public enum BooleanOperator
    {
        EQUALS,
        NOT_EQUALS,
        MORE_THAN,
        LESS_THAN,
        MORE_OR_EQUAL,
        LESS_OR_EQUAL,
        UNDEFINED
    }

    public enum MathOperation
    {
        SUM,
        SUBSTRUCT,
        MULTIPLY,
        DIVIDE,
        INCREMENT,
        DECREMENT
    }
}
