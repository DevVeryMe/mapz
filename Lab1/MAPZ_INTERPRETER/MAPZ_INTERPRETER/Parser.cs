﻿using System;
using System.Collections.Generic;

namespace MAPZ_INTERPRETER
{
    class Parser
    {
        public EventHandler<PrintEventArgs> _printEvent;
        public EventHandler<HtmlClickEventArgs> _clickEvent;
        public EventHandler<HtmlFillTextFieldEventArgs> _fillTextFieldEvent;
        public EventHandler<LoadUrlEventArgs> _loadUrlEvent;
        public EventHandler<BuildTreeEventArgs> _buildTreeEvent;

        private TokenList _tokens;
        private Dictionary<string, Variable> _variables;
        private Dictionary<string, CustomObject> _objects;
        private Block _currentBlock;
        private Stack<Block> _blockstack;
        private List<Stmt> _tree;
        private bool _running;

        public Parser(TokenList t)
        {
            _tokens = t;
            _variables = new Dictionary<string, Variable>();
            _objects = new Dictionary<string, CustomObject>();
            _currentBlock = null;
            _blockstack = new Stack<Block>();
            _tree = new List<Stmt>();
            _running = true; 
        }

        public void Invoke()
        {
            Token tok = null;
            while (_running)
            {
                try
                {
                    tok = _tokens.GetToken();
                }
                catch { }

                switch (tok.Type)
                {
                    case TokenType.EOF:
                        _running = false;
                        break;
                    case TokenType.VAR_KEYWORD:     // Parse VAR: declaration | assignment
                        ParseVar();
                        break;
                    case TokenType.OBJECT_KEYWORD:  // Parse OBJECT: declaration | assignment
                        ParseObject();
                        break;
                    case TokenType.IDENT:           // Parse existing OBJECT | VAR
                        ParseIdentifier(tok.Data);
                        break;
                    case TokenType.IF:              // Parse IF block
                        ParseIFStatement(TokenType.IF);
                        break;
                    case TokenType.ELSE_IF:         // Parse ELSE_IF block
                        ParseIFStatement(TokenType.ELSE_IF);
                        break;
                    case TokenType.ELSE:            // Parse ELSE block
                        ParseElseStatement();
                        break;
                    case TokenType.WHILE:           // Parse WHILE block
                        ParseWhileLoop();
                        break;
                    case TokenType.RIGHT_BRACE:     // Parse end of block
                        ParseEndOfBlock();
                        break;
                    case TokenType.PRINT:           // Parse Print function
                        ParsePrintFunc();
                        break;
                    case TokenType.CLICK_ELEMENT:    // Parse ClickElement function
                        ParseClickFunc();
                        break;
                    case TokenType.FILL_TEXT_FIELD:    // Parse FillTextField function
                        ParseFillTextField();
                        break;
                    case TokenType.LOAD_URL:    // Parse FillTextField function
                        ParseLoadUrl();
                        break;
                    default:
                        break;
                }
            }

            BuildTree();
        }

        private void ParseIdentifier(string identifier)
        {
            bool isVar = false;

            if (_variables.ContainsKey(identifier))
            {
                isVar = true;
            }
            else if (_objects.ContainsKey(identifier))
            {
                isVar = false;
            }
            else
            {
                throw new SyntaxException("Syntax error: not existing identifier.");
            }

            if (_tokens.Peek().Type == TokenType.ASSIGN_OP)
            {
                if (isVar)
                {
                    Variable variable = new Variable();
                    variable = _variables[identifier];
                    ParseVarAssignment(ref variable);
                    _variables[identifier] = variable;
                }
                else
                {
                    _objects[identifier] = ParseObjectAssignment(identifier);
                }
            }
            else if (_tokens.Peek().Type == TokenType.DOT)
            {
                if (!isVar)
                {
                    ParseObjectField(identifier);
                }
                else
                {
                    throw new SyntaxException("Syntax error: DOT operator after VAR IDETIFIER.");
                }
            }
            else if (_tokens.Peek().Type == TokenType.INCREMENT)
            {
                if (isVar)
                {
                    ParseVarIncrement(identifier);
                }
                else
                {
                    throw new SyntaxException("Syntax error: INCREMENTING OBJECT TYPE.");
                }
            }
            else if (_tokens.Peek().Type == TokenType.DECREMENT)
            {
                if (isVar)
                {
                    ParseVarDecrement(identifier);
                }
                else
                {
                    throw new SyntaxException("Syntax error: DECREMENTING OBJECT TYPE.");
                }
            }
        }
        private void ParseVar()
        {
            if (_tokens.Peek().Type == TokenType.IDENT)
            {
                Variable variable = new Variable();
                variable.Name = _tokens.Peek().Data;

                _tokens.MoveToNext();
                if (_tokens.Peek().Type == TokenType.SEMICOLON)
                {
                    variable.Value = null;
                    _variables.Add(variable.Name, variable);

                    Declaration declaration = new Declaration(variable.Name);
                    if (_currentBlock == null)
                    {
                        _tree.Add(declaration);
                    }
                    else
                    {
                        _blockstack.Peek().AddStmt(declaration);
                    }
                }
                else if (_tokens.Peek().Type == TokenType.ASSIGN_OP)
                {
                    ParseVarAssignment(ref variable);
                    _variables.Add(variable.Name, variable);
                }

                if (_currentBlock != null)
                {
                    _blockstack.Peek().AddVar(variable);
                }
            }
            else
            {
                throw new SyntaxException("Syntax error: missing identifier after \"var\" keyword.");
            }
        }
        private string ParseVarAssignment(ref Variable variable, bool isObjectField = false) // Used in private void ParseVar()
        {
            string rightParamName = string.Empty;

            _tokens.MoveToNext();
            if (_tokens.Peek().Type == TokenType.INT_LIT)
            {
                variable.Type = VarType.Integer;
                variable.Value = Int32.Parse(_tokens.Peek().Data);
            }
            else if (_tokens.Peek().Type == TokenType.STRING_LIT)
            {
                variable.Type = VarType.String;
                variable.Value = _tokens.Peek().Data;
            }
            else if (_tokens.Peek().Type == TokenType.IDENT)
            {
                Variable tmp = null;
                if (_variables.TryGetValue(_tokens.Peek().Data, out tmp))
                {
                    rightParamName = tmp.Name;
                    variable.Type = tmp.Type;
                    variable.Value = tmp.Value;
                }
                else if (_objects.ContainsKey(_tokens.Peek().Data))
                {
                    string objName = _tokens.Peek().Data;
                    _tokens.MoveToNext();
                    if (_tokens.Peek().Type == TokenType.DOT)
                    {
                        _tokens.MoveToNext();
                        if (_tokens.Peek().Type == TokenType.IDENT)
                        {
                            string fieldName = _tokens.Peek().Data;
                            if (_objects[objName]._fields.ContainsKey(fieldName))
                            {
                                rightParamName = objName + "." + fieldName;
                                variable.Type = _objects[objName]._fields[fieldName].Type;
                                variable.Value = _objects[objName]._fields[fieldName].Value;
                            }
                        }
                    }
                }
            }
            else
            {
                //throw new SyntaxException("Syntax error: unknown type in VAR assignment.");
            }

            ParseExpression(ref variable);

            if (!isObjectField)
            {
                Assign assign;
                if (rightParamName == string.Empty)
                {
                    if (variable.Type == VarType.Integer)
                    {
                        assign = new Assign(variable.Name, new IntLiteral((int)variable.Value));
                    }
                    else
                    {
                        assign = new Assign(variable.Name, new StringLiteral((string)variable.Value));
                    }
                }
                else
                {
                    assign = new Assign(variable.Name, new Ident(rightParamName));
                }

                if (_currentBlock == null)
                {
                    _tree.Add(assign);
                }
                else
                {
                    _blockstack.Peek().AddStmt(assign);
                }
            }

            return rightParamName;
        }
        private void ParseObject()
        {
            if (_tokens.Peek().Type == TokenType.IDENT)
            {
                CustomObject customObject = new CustomObject();
                customObject.Name = _tokens.Peek().Data;

                _tokens.MoveToNext();
                if (_tokens.Peek().Type == TokenType.SEMICOLON)
                {
                    _objects.Add(customObject.Name, customObject);

                    Declaration declaration = new Declaration(customObject.Name);
                    if (_currentBlock == null)
                    {
                        _tree.Add(declaration);
                    }
                    else
                    {
                        _blockstack.Peek().AddStmt(declaration);
                    }
                }
                else if (_tokens.Peek().Type == TokenType.ASSIGN_OP)
                {
                    customObject = ParseObjectAssignment(customObject.Name);
                    _objects.Add(customObject.Name, customObject);
                }
                else
                {
                    throw new SyntaxException("Syntax error: missing identifier after \"object\" keyword.");
                }

                if (_currentBlock != null)
                {
                    _blockstack.Peek().AddObj(customObject);
                }
            }
        }
        private CustomObject ParseObjectAssignment(string name) // Used in private void ParseObject()
        {
            CustomObject customObject = new CustomObject();
            customObject.Name = name;

            _tokens.MoveToNext();
            if (_tokens.Peek().Type == TokenType.LEFT_BRACE)
            {
                while (_tokens.Peek().Type != TokenType.RIGHT_BRACE)
                {
                    _tokens.MoveToNext();
                    if (_tokens.Peek().Type == TokenType.IDENT)
                    {
                        Variable variable = new Variable();
                        variable.Name = _tokens.Peek().Data;

                        _tokens.MoveToNext();
                        if (_tokens.Peek().Type == TokenType.ASSIGN_OP)
                        {
                            Variable tmp = new Variable();
                            tmp = variable;
                            ParseVarAssignment(ref tmp, true);
                            customObject._fields.Add(tmp.Name, tmp);
                        }
                    }
                }

                Assign assign = new Assign(customObject.Name, new ObjectBlock(customObject._fields));
                if (_currentBlock == null)
                {
                    _tree.Add(assign);
                }
                else
                {
                    _blockstack.Peek().AddStmt(assign);
                }
                _tokens.MoveToNext();
            }

            return customObject;
        }
        private void ParseObjectField(string objectName) // Used after DOT operator
        {
            _tokens.MoveToNext();
            if (_tokens.Peek().Type == TokenType.IDENT)
            {
                string name = _tokens.Peek().Data;

                _tokens.MoveToNext();
                if (_tokens.Peek().Type == TokenType.ASSIGN_OP)
                {
                    Variable variable = new Variable();
                    variable = _objects[objectName]._fields[name];
                    string rightParamIdent = ParseVarAssignment(ref variable, true);
                    _objects[objectName]._fields[name] = variable;

                    Variable tmp = _objects[objectName]._fields[name];
                    Assign assign;

                    if (rightParamIdent == string.Empty)
                    {
                        if (tmp.Type == VarType.Integer)
                        {
                            assign = new Assign((objectName + "." + name), new IntLiteral((int)tmp.Value));
                        }
                        else
                        {
                            assign = new Assign((objectName + "." + name), new StringLiteral((string)tmp.Value));
                        }
                    }
                    else
                    {
                        assign = new Assign((objectName + "." + name), new Ident(rightParamIdent));
                    }

                    if (_currentBlock == null)
                    {
                        _tree.Add(assign);
                    }
                    else
                    {
                        _blockstack.Peek().AddStmt(assign);
                    }
                }
                else if (_tokens.Peek().Type == TokenType.INCREMENT)
                {
                    if (_objects[objectName]._fields[name].Value != null)
                    {
                        _objects[objectName]._fields[name].Value = (int)_objects[objectName]._fields[name].Value + 1;
                    }

                    Assign assign = new Assign((objectName + "." + name), new MathExpr(new ObjectIdent(objectName, name), MathOperation.INCREMENT, new Expr()));
                    if (_currentBlock == null)
                    {
                        _tree.Add(assign);
                    }
                    else
                    {
                        _blockstack.Peek().AddStmt(assign);
                    }
                }
                else if (_tokens.Peek().Type == TokenType.DECREMENT)
                {
                    if (_objects[objectName]._fields[name].Value != null && (int)_objects[objectName]._fields[name].Value > 0)
                    {
                        _objects[objectName]._fields[name].Value = (int)_objects[objectName]._fields[name].Value - 1;
                    }

                    Assign assign = new Assign((objectName + "." + name), new MathExpr(new ObjectIdent(objectName, name), MathOperation.DECREMENT, new Expr()));
                    if (_currentBlock == null)
                    {
                        _tree.Add(assign);
                    }
                    else
                    {
                        _blockstack.Peek().AddStmt(assign);
                    }
                }
            }
            else
            {
                throw new SyntaxException("Syntax error: missing identifier after DOT operator.");
            }
        }
        private void ParseExpression(ref Variable variable)
        {
            _tokens.MoveToNext();
            if (variable.Type == VarType.Integer)
            {
                while (true)
                {
                    if (_tokens.Peek().Type == TokenType.SUM_OP)
                    {
                        _tokens.MoveToNext();
                        if (_tokens.Peek().Type == TokenType.INT_LIT)
                        {
                            variable.Value = (int)variable.Value + Int32.Parse(_tokens.Peek().Data);
                        }
                        else if (_tokens.Peek().Type == TokenType.IDENT)
                        {
                            Variable tmp = null;
                            if (_variables.TryGetValue(_tokens.Peek().Data, out tmp))
                            {
                                if (tmp.Type == VarType.Integer)
                                {
                                    variable.Value = (int)variable.Value + (int)tmp.Value;
                                }
                                else
                                {
                                    throw new SyntaxException("Type error: vars with different type used in one statement.");
                                }
                            }
                        }
                    }
                    else if (_tokens.Peek().Type == TokenType.SUBSTACT_OP)
                    {
                        _tokens.MoveToNext();
                        if (_tokens.Peek().Type == TokenType.INT_LIT)
                        {
                            variable.Value = (int)variable.Value - Int32.Parse(_tokens.Peek().Data);
                        }
                        else if (_tokens.Peek().Type == TokenType.IDENT)
                        {
                            Variable tmp = null;
                            if (_variables.TryGetValue(_tokens.Peek().Data, out tmp))
                            {
                                if (tmp.Type == VarType.Integer)
                                {
                                    variable.Value = (int)variable.Value - (int)tmp.Value;
                                }
                                else
                                {
                                    throw new SyntaxException("Type error: vars with different type used in one statement.");
                                }
                            }
                        }
                    }
                    else if (_tokens.Peek().Type == TokenType.MULTIPLY_OP)
                    {
                        _tokens.MoveToNext();
                        if (_tokens.Peek().Type == TokenType.INT_LIT)
                        {
                            variable.Value = (int)variable.Value * Int32.Parse(_tokens.Peek().Data);
                        }
                        else if (_tokens.Peek().Type == TokenType.IDENT)
                        {
                            Variable tmp = null;
                            if (_variables.TryGetValue(_tokens.Peek().Data, out tmp))
                            {
                                if (tmp.Type == VarType.Integer)
                                {
                                    variable.Value = (int)variable.Value * (int)tmp.Value;
                                }
                                else
                                {
                                    throw new SyntaxException("Type error: vars with different type used in one statement.");
                                }
                            }
                        }
                    }
                    else if (_tokens.Peek().Type == TokenType.DIVISION_OP)
                    {
                        _tokens.MoveToNext();
                        if (_tokens.Peek().Type == TokenType.INT_LIT)
                        {
                            variable.Value = (int)variable.Value / Int32.Parse(_tokens.Peek().Data);
                        }
                        else if (_tokens.Peek().Type == TokenType.IDENT)
                        {
                            Variable tmp = null;
                            if (_variables.TryGetValue(_tokens.Peek().Data, out tmp))
                            {
                                if (tmp.Type == VarType.Integer)
                                {
                                    variable.Value = (int)variable.Value / (int)tmp.Value;
                                }
                                else
                                {
                                    throw new SyntaxException("Type error: vars with different type used in one statement.");
                                }
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                    _tokens.MoveToNext();
                }
            }
            else if (variable.Type == VarType.String)
            {
                while (true)
                {
                    if (_tokens.Peek().Type == TokenType.SUM_OP)
                    {
                        _tokens.MoveToNext();
                        if (_tokens.Peek().Type == TokenType.STRING_LIT)
                        {
                            variable.Value = (string)variable.Value + _tokens.Peek().Data;
                        }
                        else if (_tokens.Peek().Type == TokenType.IDENT)
                        {
                            Variable tmp = null;
                            if (_variables.TryGetValue(_tokens.Peek().Data, out tmp))
                            {
                                if (tmp.Type == VarType.String)
                                {
                                    variable.Value = (string)variable.Value + (string)tmp.Value;
                                }
                                else
                                {
                                    throw new SyntaxException("Type error: vars with different type used in one statement.");
                                }
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                    _tokens.MoveToNext();
                }
            }
            else
            {
                throw new SyntaxException("Syntax error: unknown VAR type.");
            }
        }
        private void ParseVarIncrement(string idetifier)
        {
            if (_variables[idetifier].Value != null)
            {
                if (_variables[idetifier].Type == VarType.Integer)
                {
                    _variables[idetifier].Value = (int)_variables[idetifier].Value + 1;

                    Assign assign = new Assign(_variables[idetifier].Name, new MathExpr(new Ident(_variables[idetifier].Name), MathOperation.INCREMENT, new Expr()));
                    if (_currentBlock == null)
                    {
                        _tree.Add(assign);
                    }
                    else
                    {
                        _blockstack.Peek().AddStmt(assign);
                    }
                }
            }
        }
        private void ParseVarDecrement(string idetifier)
        {
            if (_variables[idetifier].Type == VarType.Integer)
            {
                if (_variables[idetifier].Value != null && (int)_variables[idetifier].Value > 0)
                {
                    _variables[idetifier].Value = (int)_variables[idetifier].Value - 1;

                    Assign assign = new Assign(_variables[idetifier].Name, new MathExpr(new Ident(_variables[idetifier].Name), MathOperation.DECREMENT, new Expr()));
                    if (_currentBlock == null)
                    {
                        _tree.Add(assign);
                    }
                    else
                    {
                        _blockstack.Peek().AddStmt(assign);
                    }
                }
            }
        }
        private void SkipBlock()
        {
            _blockstack.Pop();
            if (_blockstack.Count == 0)
            {
                _currentBlock = null;
            }

            do
            {
                _tokens.MoveToNext();
                if(_tokens.Peek().Type == TokenType.LEFT_BRACE)
                {
                    _blockstack.Push(new Block());
                    SkipBlock();
                }
            }
            while (_tokens.Peek().Type != TokenType.RIGHT_BRACE);
            _tokens.MoveToNext();
        }
        private void ReverseSkipBlock()
        {
            do
            {
                _tokens.MoveToPrev();
                if (_tokens.Peek().Type == TokenType.RIGHT_BRACE)
                {
                    ReverseSkipBlock();
                }
            }
            while (_tokens.Peek().Type != TokenType.LEFT_BRACE);
            _tokens.MoveToPrev();
        }
        private void SkipElseIfBlocks()
        {
            if (_tokens.Peek().Type == TokenType.ELSE_IF || _tokens.Peek().Type == TokenType.ELSE)
            {
                if (_tokens.Peek().Type == TokenType.ELSE_IF)
                {
                    _blockstack.Push(new IfBlock());
                }
                else
                {
                    _blockstack.Push(new ElseBlock());
                }

                MoveToBlockStart();
                SkipBlock();
                SkipElseIfBlocks();
            }
        }
        private void ParseCondition()
        {
            if (_tokens.Peek().Type == TokenType.LEFT_PARAN)
            {
                object leftParam = null;
                BooleanOperator op = BooleanOperator.UNDEFINED;

                string leftParamFullIdent = ParseConditionLeftParam(ref leftParam, ref op);
                ParseConditionRightParam(leftParam, op, leftParamFullIdent);
            }
            else
            {
                throw new SyntaxException("Syntax error: missing \'(\' after \"if\" keyword.");
            }
        }
        private void ParseIFStatement(TokenType token)
        {
            if (token == TokenType.IF)
            {
                _blockstack.Push(new IfBlock());
            }
            else
            {
                _blockstack.Push(new ElseIfBlock());
            }
            _currentBlock = _blockstack.Peek();

            ParseCondition();
        }
        private void ParseElseStatement()
        {
            _blockstack.Push(new ElseBlock());
            _currentBlock = _blockstack.Peek();
        }
        private void ParseComparison(Variable leftVar, Variable rightVar, BooleanOperator op, string leftParamFullIdent)
        {
            if (leftVar.Type == rightVar.Type)
            {
                MoveToBlockStart();

                bool isBlockSkipped = false;
                if (leftVar.Type == VarType.Integer)
                {
                    int leftValue = (int)leftVar.Value;
                    int rightValue = (int)rightVar.Value;

                    if (op == BooleanOperator.EQUALS)
                    {
                        if (leftValue != rightValue)
                        {
                            isBlockSkipped = true;
                            SkipBlock();
                        }
                    }
                    else if (op == BooleanOperator.NOT_EQUALS)
                    {
                        if (leftValue == rightValue)
                        {
                            isBlockSkipped = true;
                            SkipBlock();
                        }
                    }
                    else if (op == BooleanOperator.MORE_THAN)
                    {
                        if (leftValue <= rightValue)
                        {
                            isBlockSkipped = true;
                            SkipBlock();
                        }
                    }
                    else if (op == BooleanOperator.LESS_THAN)
                    {
                        if (leftValue >= rightValue)
                        {
                            isBlockSkipped = true;
                            SkipBlock();
                        }
                    }
                    else if (op == BooleanOperator.MORE_OR_EQUAL)
                    {
                        if (leftValue < rightValue)
                        {
                            isBlockSkipped = true;
                            SkipBlock();
                        }
                    }
                    else if (op == BooleanOperator.LESS_OR_EQUAL)
                    {
                        if (leftValue > rightValue)
                        {
                            isBlockSkipped = true;
                            SkipBlock();
                        }
                    }
                    else
                    {
                        throw new SyntaxException("Syntax error: not existing operator inside IF statement.");
                    }

                    if (!isBlockSkipped)
                    {
                        (_blockstack.Peek() as ConditionBlock).Operator = op;
                        if (leftVar.Name == string.Empty)
                        {
                            (_blockstack.Peek() as ConditionBlock).LeftExpr = new IntLiteral(leftValue);
                            if (rightVar.Name == string.Empty)
                            {
                                (_blockstack.Peek() as ConditionBlock).RightExpr = new IntLiteral(rightValue);
                            }
                            else
                            {
                                (_blockstack.Peek() as ConditionBlock).RightExpr = new Ident(rightVar.Name);
                            }
                        }
                        else
                        {
                            (_blockstack.Peek() as ConditionBlock).LeftExpr = new Ident(leftParamFullIdent);
                            if (rightVar.Name == string.Empty)
                            {
                                (_blockstack.Peek() as ConditionBlock).RightExpr = new IntLiteral(rightValue);
                            }
                            else
                            {
                                (_blockstack.Peek() as ConditionBlock).RightExpr = new Ident(rightVar.Name);
                            }
                        }
                    }
                }
                else if (leftVar.Type == VarType.String)
                {
                    string leftValue = (string)leftVar.Value;
                    string rightValue = (string)rightVar.Value;

                    if (op == BooleanOperator.EQUALS)
                    {
                        if (!leftValue.Equals(rightValue))
                        {
                            isBlockSkipped = true;
                            SkipBlock();
                        }
                    }
                    else if (op == BooleanOperator.NOT_EQUALS)
                    {
                        if (leftValue.Equals(rightValue))
                        {
                            isBlockSkipped = true;
                            SkipBlock();
                        }
                    }
                    else
                    {
                        throw new SyntaxException("Syntax error: not existing operator inside IF statement.");
                    }

                    if (!isBlockSkipped)
                    {
                        (_blockstack.Peek() as ConditionBlock).Operator = op;
                        if (leftVar.Name == string.Empty)
                        {
                            (_blockstack.Peek() as ConditionBlock).LeftExpr = new StringLiteral(leftValue);
                            if (rightVar.Name == string.Empty)
                            {
                                (_blockstack.Peek() as ConditionBlock).RightExpr = new StringLiteral(rightValue);
                            }
                            else
                            {
                                (_blockstack.Peek() as ConditionBlock).RightExpr = new Ident(rightVar.Name);
                            }
                        }
                        else
                        {
                            (_blockstack.Peek() as ConditionBlock).LeftExpr = new Ident(leftParamFullIdent);
                            if (rightVar.Name == string.Empty)
                            {
                                (_blockstack.Peek() as ConditionBlock).RightExpr = new StringLiteral(rightValue);
                            }
                            else
                            {
                                (_blockstack.Peek() as ConditionBlock).RightExpr = new Ident(rightVar.Name);
                            }
                        }
                    }
                }
                else
                {
                    throw new SyntaxException("Syntax error: undefined type operands inside IF statement");
                }

                if (!isBlockSkipped)
                {
                    _tokens.MoveToNext();
                }
            }
            else
            {
                throw new SyntaxException("Syntax error: different type operands inside IF statement");
            }
        }
        private void GetBooleanOperator(out BooleanOperator op)
        {
            _tokens.MoveToNext();
            if (_tokens.Peek().Type == TokenType.EQUALS)
            {
                op = BooleanOperator.EQUALS;
            }
            else if (_tokens.Peek().Type == TokenType.NOT_EQUALS)
            {
                op = BooleanOperator.NOT_EQUALS;
            }
            else if (_tokens.Peek().Type == TokenType.MORE_THAN)
            {
                op = BooleanOperator.MORE_THAN;
            }
            else if (_tokens.Peek().Type == TokenType.LESS_THAN)
            {
                op = BooleanOperator.LESS_THAN;
            }
            else if (_tokens.Peek().Type == TokenType.MORE_OR_EQUAL)
            {
                op = BooleanOperator.MORE_OR_EQUAL;
            }
            else if (_tokens.Peek().Type == TokenType.LESS_OR_EQUAL)
            {
                op = BooleanOperator.LESS_OR_EQUAL;
            }
            else
            {
                op = BooleanOperator.UNDEFINED;
            }
        }
        private string ParseConditionLeftParam(ref object leftParam, ref BooleanOperator op)
        {
            string leftParamFullIdent = string.Empty;
            _tokens.MoveToNext();
            if (_tokens.Peek().Type == TokenType.IDENT)
            {
                if (_variables.ContainsKey(_tokens.Peek().Data))
                {
                    leftParam = _variables[_tokens.Peek().Data];
                    leftParamFullIdent = (leftParam as Variable).Name;

                    GetBooleanOperator(out op);
                }
                else if (_objects.ContainsKey(_tokens.Peek().Data))
                {
                    CustomObject leftObject = _objects[_tokens.Peek().Data];
                    _tokens.MoveToNext();
                    if (_tokens.Peek().Type == TokenType.DOT)
                    {
                        _tokens.MoveToNext();
                        if (_tokens.Peek().Type == TokenType.IDENT)
                        {
                            if (leftObject._fields.ContainsKey(_tokens.Peek().Data))
                            {
                                leftParam = leftObject._fields[_tokens.Peek().Data];
                                leftParamFullIdent = leftObject.Name + "." + (leftParam as Variable).Name;

                                GetBooleanOperator(out op);
                            }
                            else
                            {
                                throw new SyntaxException("Syntax error: unknown identifier after DOT operator.");
                            }
                        }
                        else
                        {
                            throw new SyntaxException("Syntax error: missing identifier after DOT operator.");
                        }
                    }
                }
                else
                {
                    throw new SyntaxException("Syntax error: not existing identifier.");
                }
            }
            else if (_tokens.Peek().Type == TokenType.INT_LIT)
            {
                leftParam = new Variable();

                (leftParam as Variable).Type = VarType.Integer;
                (leftParam as Variable).Value = Int32.Parse(_tokens.Peek().Data);
                GetBooleanOperator(out op);
            }
            else if (_tokens.Peek().Type == TokenType.STRING_LIT)
            {
                leftParam = new Variable();

                (leftParam as Variable).Type = VarType.String;
                (leftParam as Variable).Value = _tokens.Peek().Data;
                GetBooleanOperator(out op);
            }
            else
            {
                throw new SyntaxException("Syntax error: unknown parameter inside IF statement.");
            }

            return leftParamFullIdent;
        }
        private void ParseConditionRightParam(object leftParam, BooleanOperator op, string leftParamFullIdent)
        {
            object rightParam = null;

            _tokens.MoveToNext();
            if (_tokens.Peek().Type == TokenType.IDENT)
            {
                if (_variables.ContainsKey(_tokens.Peek().Data))
                {
                    rightParam = _variables[_tokens.Peek().Data];

                    Variable leftVar = (leftParam as Variable);
                    Variable rightVar = (rightParam as Variable);
                    ParseComparison(leftVar, rightVar, op, leftParamFullIdent);
                }
                else if (_objects.ContainsKey(_tokens.Peek().Data))
                {
                    rightParam = _objects[_tokens.Peek().Data];

                    _tokens.MoveToNext();
                    if (_tokens.Peek().Type == TokenType.DOT)
                    {
                        _tokens.MoveToNext();
                        if (_tokens.Peek().Type == TokenType.IDENT)
                        {
                            if ((rightParam as CustomObject)._fields.ContainsKey(_tokens.Peek().Data))
                            {
                                Variable leftVar = (leftParam as Variable);
                                Variable rightVar = (rightParam as CustomObject)._fields[_tokens.Peek().Data];

                                ParseComparison(leftVar, rightVar, op, leftParamFullIdent);
                            }
                        }
                        else
                        {
                            throw new SyntaxException("Syntax error: missing identifier after DOT operator.");
                        }
                    }
                }
                else
                {
                    throw new SyntaxException("Syntax error: not existing identifier.");
                }
            }
            else if (_tokens.Peek().Type == TokenType.INT_LIT)
            {
                Variable leftVar = (leftParam as Variable);

                ParseComparison(leftVar, new Variable(string.Empty, Int32.Parse(_tokens.Peek().Data), VarType.Integer), op, leftParamFullIdent);
            }
            else if (_tokens.Peek().Type == TokenType.STRING_LIT)
            {
                Variable leftVar = (leftParam as Variable);

                ParseComparison(leftVar, new Variable(string.Empty, _tokens.Peek().Data, VarType.String), op, leftParamFullIdent);
            }
        }
        private void MoveToBlockStart()
        {
            while (_tokens.Peek().Type != TokenType.LEFT_BRACE)
            {
                _tokens.MoveToNext();
            }
        }
        private void MoveToEndOfExpr()
        {
            do
            {
                _tokens.MoveToNext();
            }
            while (_tokens.Peek().Type != TokenType.SEMICOLON);
            _tokens.MoveToNext();
        }
        private void ParseEndOfBlock()
        {
            _currentBlock = _blockstack.Pop();
            DeleteBlockLocals();

            if (_currentBlock is IfBlock || _currentBlock is ElseIfBlock)
            {
                SkipElseIfBlocks();
            }
            else if (_currentBlock is WhileBlock)
            {
                _tokens.MoveToPrev();
                ReverseSkipBlock();
                ResetLoop();
            }

            if (_blockstack.Count == 0)
            {
                _tree.Add(_currentBlock);
                _currentBlock = null;
            }
            else
            {
                _blockstack.Peek().AddStmt(_currentBlock);
            }
        }
        private void ParseWhileLoop()
        {
            _blockstack.Push(new WhileBlock());
            _currentBlock = _blockstack.Peek();

            ParseCondition();
        }
        private void ResetLoop()
        {
            do
            {
                _tokens.MoveToPrev();
            }
            while (_tokens.Peek().Type != TokenType.WHILE);
        }
        private void ParsePrintFunc()
        {
            List<Expr> argList = new List<Expr>();
            string output = ParsePrintArgs(ref argList);
            output = output.Replace("\"", string.Empty);

            PrintEventArgs args = new PrintEventArgs(output);
            _printEvent.Invoke(this, args);

            Call call = new Call("Print", argList);

            if (_currentBlock == null)
            {
                _tree.Add(call);
            }
            else
            {
                _blockstack.Peek().AddStmt(call);
            }
        }
        private string ParsePrintArgs(ref List<Expr> argList)
        {
            Variable variable = new Variable();
            variable.Type = VarType.String;

            _tokens.MoveToNext();
            if(_tokens.Peek().Type == TokenType.STRING_LIT)
            {
                variable.Value = _tokens.Peek().Data;
            }
            else if (_tokens.Peek().Type == TokenType.IDENT)
            {
                bool isVar = false;
                string identifier = _tokens.Peek().Data;
                variable.Name = identifier;

                if (_variables.ContainsKey(identifier))
                {
                    isVar = true;
                }
                else if (_objects.ContainsKey(identifier))
                {
                    isVar = false;
                }
                else
                {
                    throw new SyntaxException("Syntax error: not existing identifier.");
                }

                if(isVar)
                {
                    if (_variables[identifier].Type == VarType.Integer)
                    {
                        variable.Value = ((int)_variables[identifier].Value).ToString();
                    }
                    else
                    {
                        variable.Value = (string)_variables[identifier].Value;
                    }
                }
                else
                {
                    _tokens.MoveToNext();
                    if (_tokens.Peek().Type == TokenType.DOT)
                    {
                        _tokens.MoveToNext();
                        if (_tokens.Peek().Type == TokenType.IDENT)
                        {
                            variable.Name += "." + _tokens.Peek().Data;
                            if (_objects[identifier]._fields[_tokens.Peek().Data].Type == VarType.Integer)
                            {
                                variable.Value = ((int)_objects[identifier]._fields[_tokens.Peek().Data].Value).ToString();
                            }
                            else
                            {
                                variable.Value = (string)_objects[identifier]._fields[_tokens.Peek().Data].Value;
                            }
                        }
                    }
                    else
                    {
                        throw new SyntaxException("Syntax error: Can't print object type.");
                    }
                }
            }
            else
            {
                throw new SyntaxException("Syntax error: Print() bad args.");
            }

            ParseExpression(ref variable);
            MoveToEndOfExpr();

            if (variable.Name == string.Empty)
            {
                variable.Value = ((string)variable.Value).Replace("\"", string.Empty);
                argList.Add(new StringLiteral((string)variable.Value));
            }
            else
            {
                argList.Add(new Ident(variable.Name));
            }

            return (string)variable.Value; 
        }
        private void ParseClickFunc()
        {
            if (_tokens.Peek().Type == TokenType.LEFT_PARAN)
            {
                string ident;
                List<Expr> argList = new List<Expr>();
                FindBy attribute;

                ParseClickArgs(out ident, out attribute, ref argList);

                _clickEvent.Invoke(this, new HtmlClickEventArgs(ident, attribute));

                Call call = new Call("Click", argList);

                if (_currentBlock == null)
                {
                    _tree.Add(call);
                }
                else
                {
                    _blockstack.Peek().AddStmt(call);
                }
            }
        }
        private void ParseClickArgs(out string data, out FindBy attribute, ref List<Expr> argList)
        {
            data = string.Empty;
            attribute = FindBy.UNDEFINED;

            string secondArg;
            argList.Add(ParseStringArg(out data));

            _tokens.MoveToNext();
            if(_tokens.Peek().Type == TokenType.COMMA)
            {
                argList.Add(ParseStringArg(out secondArg));

                secondArg = secondArg.Replace("\"", string.Empty);
                switch(secondArg)
                {
                    case "id":
                        attribute = FindBy.ID;
                        break;
                    case "linkText":
                        attribute = FindBy.LinkText;
                        break;
                    case "name":
                        attribute = FindBy.NAME;
                        break;
                    case "class":
                        attribute = FindBy.CLASS;
                        break;
                    default:
                        break;
                }
            }
            else
            {
                throw new SyntaxException("Syntax error: missing second arg in Click function.");
            }
        }
        private Expr ParseStringArg(out string data)
        {
            Expr expr = null;
            data = string.Empty;
            _tokens.MoveToNext();
            if (_tokens.Peek().Type == TokenType.STRING_LIT)
            {
                data = _tokens.Peek().Data;
                expr = new StringLiteral(data.Replace("\"", string.Empty));
            }
            else if (_tokens.Peek().Type == TokenType.IDENT)
            {
                bool isVar = false;
                string identifier = _tokens.Peek().Data;

                if (_variables.ContainsKey(identifier))
                {
                    isVar = true;
                }
                else if (_objects.ContainsKey(identifier))
                {
                    isVar = false;
                }
                else
                {
                    throw new SyntaxException("Syntax error: not existing identifier.");
                }

                if (isVar)
                {
                    if (_variables[identifier].Type == VarType.String)
                    {
                        data = (string)_variables[identifier].Value;
                        expr = new Ident(identifier);
                    }
                    else
                    {
                        throw new SyntaxException("Syntax error: function wrong args.");
                    }
                }
                else
                {
                    _tokens.MoveToNext();
                    if (_tokens.Peek().Type == TokenType.DOT)
                    {
                        _tokens.MoveToNext();
                        if (_tokens.Peek().Type == TokenType.IDENT)
                        {
                            Variable tmp = _objects[identifier]._fields[_tokens.Peek().Data];
                            if (tmp.Type == VarType.String)
                            {
                                data = (string)_objects[identifier]._fields[_tokens.Peek().Data].Value;
                                expr = new ObjectIdent(identifier, _tokens.Peek().Data);
                            }
                            else
                            {
                                throw new SyntaxException("Syntax error: function wrong args.");
                            }
                        }
                    }
                    else
                    {
                        throw new SyntaxException("Syntax error: Can't print object type.");
                    }
                }
            }
            else
            {
                throw new SyntaxException("Syntax error: function wrong args.");
            }

            data = data.Replace("\"", string.Empty);
            return expr;
        }
        private void ParseFillTextField()
        {
            if (_tokens.Peek().Type == TokenType.LEFT_PARAN)
            {
                string data;
                List<Expr> argList = new List<Expr>();
                FindBy attribute;
                string content;

                ParseClickArgs(out data, out attribute, ref argList);

                _tokens.MoveToNext();
                if (_tokens.Peek().Type == TokenType.COMMA)
                {
                    argList.Add(ParseStringArg(out content));
                }
                else
                {
                    throw new SyntaxException("Syntax error: too few args in FillTextField function call.");
                }

                _fillTextFieldEvent.Invoke(this, new HtmlFillTextFieldEventArgs(data, attribute, content));

                Call call = new Call("FillTextField", argList);

                if (_currentBlock == null)
                {
                    _tree.Add(call);
                }
                else
                {
                    _blockstack.Peek().AddStmt(call);
                }
            }
        }
        private void ParseLoadUrl()
        {
            string url;
            List<Expr> argList = new List<Expr>();

            argList.Add(ParseStringArg(out url));
            _loadUrlEvent.Invoke(this, new LoadUrlEventArgs(url));

            Call call = new Call("LoadUrl", argList);

            if (_currentBlock == null)
            {
                _tree.Add(call);
            }
            else
            {
                _blockstack.Peek().AddStmt(call);
            }
        }
        private void DeleteBlockLocals()
        {
            foreach (var variable in _currentBlock.Variables)
            {
                _variables.Remove(variable.Name);
            }
            foreach (var obj in _currentBlock.Objects)
            {
                _objects.Remove(obj.Name);
            }
        }
        private void BuildTree()
        {
            _buildTreeEvent.Invoke(this, new BuildTreeEventArgs(_tree));
        }
    }
}