﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

/*Test Code*/
/*
var test1 = 20;
var test2 = 2 * 5 + test1;
var subname = "Subname";
 
object TestObj = { Name = "VeryMe", Age = 18, Count = 10 };
    
while (TestObj.Count > 0)
{
	var counter = 5;
    if (test1 < TestObj.Age)
    {
        test1 = TestObj.Age;
    }
    else_if (TestObj.Name == "VeryMe")
    {
        TestObj.Name = subname;
        TestObj.Name = TestObj.Name;
        TestObj.Name = "Veremiy";
    }
    else
    {
        test2--;
    }

    if (5 > 3)
    {
        test1++;
        test1 = test1;
    }

    TestObj.Count--;
    Print(TestObj.Count);
    
    while (counter > 0)
    {
    	counter--;
    	Print("Hellooo!");
    }
}
FillTextField("comment", "id", "Text about me.");
ClickElement("user", "id");
*/

namespace MAPZ_INTERPRETER
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    
    public enum FindBy
    {
        ID,
        NAME,
        CLASS,
        LinkText,
        UNDEFINED
    }
    public class PrintEventArgs : EventArgs
    {
        private string _output;
        public string Output 
        { 
            get
            {
                return _output;
            }
        }
        public PrintEventArgs(params string[] list)
        {
            foreach (string str in list)
            {
                _output += str;
            }
        }
    }
    public class HtmlClickEventArgs : EventArgs
    {
        private readonly string _data;
        public FindBy Attribute { get; set; }
        public string Data
        {
            get
            {
                return _data;
            }
        }
        public HtmlClickEventArgs(string data, FindBy attribute)
        {
            _data = data;
            Attribute = attribute;
        }
    }
    public class HtmlFillTextFieldEventArgs : EventArgs
    {
        private readonly string _data;
        private readonly string _content;
        public FindBy Attribute { get; set; }
        public string Data
        {
            get
            {
                return _data;
            }
        }
        public string Content
        {
            get
            {
                return _content;
            }
        }
        public HtmlFillTextFieldEventArgs(string data, FindBy attribute, string content)
        {
            _data = data;
            Attribute = attribute;
            _content = content;
        }
    }
    public class LoadUrlEventArgs : EventArgs
    {
        private readonly string _url;
        public string Url
        {
            get
            {
                return _url;
            }
        }
        public LoadUrlEventArgs(string url)
        {
            _url = url;
        }
    }
    public class BuildTreeEventArgs : EventArgs
    {
        private List<Stmt> _tree;

        public List<Stmt> Tree
        {
            get
            {
                return _tree;
            }
        }
        public BuildTreeEventArgs(List<Stmt> tree)
        {
            _tree = tree;
        }
    }
    public partial class MainWindow : Window
    {
        private ChromeDriver _chromeDriver;
        public MainWindow()
        {
            InitializeComponent();
            InitializeBrowser();
        }
        private void CompileButton_Click(object sender, RoutedEventArgs e)
        {
            this.TextBlockOutput.Text = string.Empty;
            string input = this.InputCode.Text;

            Tokenizer tokenizer = new Tokenizer(input);

            List<Token> tokens = new List<Token>();
            while (true)
            {
                Token token = tokenizer.GetToken();

                if (token.Type == TokenType.EOF)
                {
                    tokens.Add(token);
                    break;
                }
                else if (token.Type != TokenType.WHITE_SPACE && token.Type != TokenType.NEW_LINE && token.Type != TokenType.COMMENT && token.Type != TokenType.UNDEFINED)
                {
                    tokens.Add(token);
                }
            }

            Parser parser = new Parser(new TokenList(tokens));
            parser._printEvent += PrintText;
            parser._clickEvent += ParseSendClick;
            parser._fillTextFieldEvent += ParseTextField;
            parser._loadUrlEvent += ParseLoadUrl;
            parser._buildTreeEvent += BuildTree;
            parser._buildTreeEvent += BuildTreeFirstOptimization;
            parser.Invoke();
        }
        private void InitializeBrowser()
        {
            _chromeDriver = new ChromeDriver(AppDomain.CurrentDomain.BaseDirectory);
            _chromeDriver.Navigate().GoToUrl("https://www.google.com");
            this.PageSource.Text = _chromeDriver.PageSource;
        }
        private void PrintText(object sender, PrintEventArgs e)
        {
            this.TextBlockOutput.Text += e.Output;
            this.TextBlockOutput.Text += "\n";
        }
        private void ParseSendClick(object sender, HtmlClickEventArgs e)
        {
            switch (e.Attribute)
            {
                case FindBy.ID:
                    SendClickById(e.Data);
                    break;
                case FindBy.LinkText:
                    SendClickByLinkText(e.Data);
                    break;
                case FindBy.NAME:
                    SendClickByName(e.Data);
                    break;
                case FindBy.CLASS:
                    SendClickByClass(e.Data);
                    break;
                default:
                    break;
            }
        }
        private void SendClickById(string attrId)
        {
            IWebElement element = _chromeDriver.FindElement(By.Id(attrId));
            element.Click();
        }
        private void SendClickByLinkText(string attrLinkText)
        {
            IWebElement element = _chromeDriver.FindElement(By.LinkText(attrLinkText));
            element.Click();
        }
        private void SendClickByName(string attrName)
        {
            IWebElement element = _chromeDriver.FindElement(By.Name(attrName));
            element.Click();
        }
        private void SendClickByClass(string attrClass)
        {
            ICollection<IWebElement> elements = _chromeDriver.FindElements(By.ClassName(attrClass));

            foreach (var element in elements)
            {
                element.Click();
            }
        }

        private void ParseTextField(object sender, HtmlFillTextFieldEventArgs e)
        {
            switch (e.Attribute)
            {
                case FindBy.ID:
                    FillTextFieldById(e.Data, e.Content);
                    break;
                case FindBy.NAME:
                    FillTextFieldByName(e.Data, e.Content);
                    break;
                case FindBy.CLASS:
                    FillTextFieldsByClass(e.Data, e.Content);
                    break;
                default:
                    break;
            }
        }
        private void FillTextFieldById(string attrId, string content)
        {
            IWebElement element = _chromeDriver.FindElement(By.Id(attrId));
            element.SendKeys(content);
        }
        private void FillTextFieldByName(string attrName, string content)
        {
            IWebElement element = _chromeDriver.FindElement(By.Name(attrName));
            element.SendKeys(content);
        }
        private void FillTextFieldsByClass(string attrClass, string content)
        {
            ICollection<IWebElement> elements = _chromeDriver.FindElements(By.ClassName(attrClass));
            
            foreach(var element in elements)
            {
                element.SendKeys(content);
            }
        }

        private void ParseLoadUrl(object sender, LoadUrlEventArgs e)
        {
            _chromeDriver.Navigate().GoToUrl(e.Url);
            this.PageSource.Text = _chromeDriver.PageSource;
        }

        private void BuildTree(object sender, BuildTreeEventArgs e)
        {
            List<Stmt> tree = e.Tree;
            TreeViewItem masterBlock = new TreeViewItem();
            masterBlock.Header = "MasterBlock";

            foreach (var node in tree)
            {
                ParseNode(node, ref masterBlock);
            }

            this.BaseTree.Items.Add(masterBlock);
        }
        private void ParseNode(Stmt node, ref TreeViewItem masterItem)
        {
            TreeViewItem item = new TreeViewItem();
            item.Header = node.ToString().Replace("MAPZ_INTERPRETER.", string.Empty);
            string leftStr = string.Empty;
            string rightStr = string.Empty;

            if (node is ConditionBlock)
            {
                TreeViewItem subitem = new TreeViewItem();
                ConditionBlock conditionBlock = (node as ConditionBlock);
                if (conditionBlock.LeftExpr is IntLiteral)
                {
                    leftStr = (conditionBlock.LeftExpr as IntLiteral).Value.ToString();
                    if (conditionBlock.RightExpr is IntLiteral)
                    {
                        rightStr = (conditionBlock.RightExpr as IntLiteral).Value.ToString();
                    }
                    else if (conditionBlock.RightExpr is Ident)
                    {
                        rightStr = (conditionBlock.RightExpr as Ident).Value;
                    }
                }
                else if (conditionBlock.LeftExpr is StringLiteral)
                {
                    leftStr = (conditionBlock.LeftExpr as StringLiteral).Value;
                    if (conditionBlock.RightExpr is StringLiteral)
                    {
                        rightStr = (conditionBlock.RightExpr as StringLiteral).Value;
                    }
                    else if (conditionBlock.RightExpr is Ident)
                    {
                        rightStr = (conditionBlock.RightExpr as Ident).Value;
                    }
                }
                else if (conditionBlock.LeftExpr is Ident)
                {
                    leftStr = (conditionBlock.LeftExpr as Ident).Value;
                    if (conditionBlock.RightExpr is IntLiteral)
                    {
                        rightStr = (conditionBlock.RightExpr as IntLiteral).Value.ToString();
                    }
                    else if (conditionBlock.RightExpr is StringLiteral)
                    {
                        rightStr = (conditionBlock.RightExpr as StringLiteral).Value;
                    }
                    else if (conditionBlock.RightExpr is Ident)
                    {
                        rightStr = (conditionBlock.RightExpr as Ident).Value;
                    }
                }

                subitem.Header = leftStr + " " + (node as ConditionBlock).GetOperator + " " + rightStr;
                foreach (var subnode in (node as ConditionBlock).Statements)
                {
                    ParseNode(subnode, ref subitem);
                }

                item.Items.Add(subitem);
            }
            else if (node is Block)
            {
                foreach (var subnode in (node as Block).Statements)
                {
                    ParseNode(subnode, ref item);
                }
            }
            else
            {
                Stmt statement = (node as Stmt);
                string result = string.Empty;
                if (statement is Declaration)
                {
                    Declaration declaration = (statement as Declaration);
                    result += declaration.Ident;
                }
                else if (statement is Assign)
                {
                    Assign assign = (statement as Assign);
                    result += assign.Ident + " = ";

                    if (assign.Value is IntLiteral)
                    {
                        result += (assign.Value as IntLiteral).Value.ToString();
                    }
                    else if (assign.Value is StringLiteral)
                    {
                        result += (assign.Value as StringLiteral).Value;
                    }
                    else if (assign.Value is Ident)
                    {
                        result += (assign.Value as Ident).Value;
                    }
                    else if (assign.Value is ObjectIdent)
                    {
                        result += (assign.Value as ObjectIdent).Name + "." + (assign.Value as ObjectIdent).Field;
                    }
                    else if (assign.Value is ObjectBlock)
                    {
                        ObjectBlock objectBlock = (assign.Value as ObjectBlock);
                        result += "{ ";

                        foreach (var arg in objectBlock.Fields)
                        {
                            result += arg.Key + " = " + arg.Value.Value.ToString() + ", "; 
                        }
                        int indexToRemove = result.LastIndexOf(", ");
                        result = result.Substring(0, indexToRemove);
                        result += " }";
                    }
                    else if (assign.Value is MathExpr)
                    {
                        MathExpr expr = (assign.Value as MathExpr);
                        if (expr.Operation == MathOperation.DECREMENT)
                        {
                            result = result.Replace(" = ", "--");
                        }
                        else if (expr.Operation == MathOperation.INCREMENT)
                        {
                            result = result.Replace(" = ", "++");
                        }
                    }
                }
                else if (statement is Call)
                {
                    Call call = (statement as Call);
                    result += call.Ident + "(";

                    foreach (var arg in call.Args)
                    {
                        if (arg is IntLiteral)
                        {
                            result += "\"" +  (arg as IntLiteral).Value.ToString() + "\"" + ", ";
                        }
                        else if (arg is StringLiteral)
                        {
                            result += "\"" + (arg as StringLiteral).Value + "\"" + ", ";
                        }
                        else if (arg is Ident)
                        {
                            result += (arg as Ident).Value + ", ";
                        }
                    }
                    int indexToRemove = result.LastIndexOf(", ");
                    result = result.Substring(0, indexToRemove);
                    result += ")";
                }

                item.Items.Add(result);
            }
            masterItem.Items.Add(item);
        }
        private void BuildTreeFirstOptimization(object sender, BuildTreeEventArgs e)
        {
            List<Stmt> tree = e.Tree;
            TreeViewItem masterBlock = new TreeViewItem();
            masterBlock.Header = "MasterBlock";

            foreach (var node in tree)
            {
                ParseFirstOptimizationNode(node, ref masterBlock);
            }
            this.OptimizedTree.Items.Add(masterBlock);
        }
        private void ParseFirstOptimizationNode(Stmt node, ref TreeViewItem masterItem)
        {
            TreeViewItem item = new TreeViewItem();
            item.Header = node.ToString().Replace("MAPZ_INTERPRETER.", string.Empty);
            string leftStr = string.Empty;
            string rightStr = string.Empty;
            bool isUseless = false;
            bool isConditionBlock = false;

            if (node is ConditionBlock)
            {
                isConditionBlock = true;
                TreeViewItem subitem = new TreeViewItem();
                ConditionBlock conditionBlock = (node as ConditionBlock);
                if (conditionBlock.LeftExpr is IntLiteral)
                {
                    leftStr = (conditionBlock.LeftExpr as IntLiteral).Value.ToString();
                    if (conditionBlock.RightExpr is IntLiteral)
                    {
                        isUseless = true;
                    }
                    else if (conditionBlock.RightExpr is Ident)
                    {
                        rightStr = (conditionBlock.RightExpr as Ident).Value;
                    }
                }
                else if (conditionBlock.LeftExpr is StringLiteral)
                {
                    leftStr = (conditionBlock.LeftExpr as StringLiteral).Value;
                    if (conditionBlock.RightExpr is StringLiteral)
                    {
                        isUseless = true;
                    }
                    else if (conditionBlock.RightExpr is Ident)
                    {
                        rightStr = (conditionBlock.RightExpr as Ident).Value;
                    }
                }
                else if (conditionBlock.LeftExpr is Ident)
                {
                    leftStr = (conditionBlock.LeftExpr as Ident).Value;
                    if (conditionBlock.RightExpr is IntLiteral)
                    {
                        rightStr = (conditionBlock.RightExpr as IntLiteral).Value.ToString();
                    }
                    else if (conditionBlock.RightExpr is StringLiteral)
                    {
                        rightStr = (conditionBlock.RightExpr as StringLiteral).Value;
                    }
                    else if (conditionBlock.RightExpr is Ident)
                    {
                        if ((conditionBlock.LeftExpr as Ident).Value == (conditionBlock.RightExpr as Ident).Value)
                        {
                            isUseless = true;
                        }
                        else
                        {
                            rightStr = (conditionBlock.RightExpr as Ident).Value;
                        }
                    }
                }

                if (!isUseless)
                {
                    subitem.Header = leftStr + " " + (node as ConditionBlock).GetOperator + " " + rightStr;
                }
                else
                {
                    subitem.Header = "Block";
                }

                foreach (var subnode in (node as ConditionBlock).Statements)
                {
                    ParseFirstOptimizationNode(subnode, ref subitem);
                }

                if (!isUseless)
                {
                    item.Items.Add(subitem);
                }
                else
                {
                    item = subitem;
                }
            }
            else if (node is Block)
            {
                Block conditionBlock = (node as Block);
                foreach (var subnode in (node as Block).Statements)
                {
                    ParseFirstOptimizationNode(subnode, ref item);
                }
            }
            else
            {
                Stmt statement = (node as Stmt);
                string result = string.Empty;
                if (statement is Declaration)
                {
                    Declaration declaration = (statement as Declaration);
                    result += declaration.Ident;
                }
                else if (statement is Assign)
                {
                    Assign assign = (statement as Assign);
                    result += assign.Ident + " = ";

                    if (assign.Value is IntLiteral)
                    {
                        result += (assign.Value as IntLiteral).Value.ToString();
                    }
                    else if (assign.Value is StringLiteral)
                    {
                        result += (assign.Value as StringLiteral).Value;
                    }
                    else if (assign.Value is Ident)
                    {
                        if (assign.Ident == (assign.Value as Ident).Value)
                        {
                            isUseless = true;
                        }
                        else
                        {
                            result += (assign.Value as Ident).Value;
                        }
                    }
                    else if (assign.Value is ObjectIdent)
                    {
                        if (assign.Ident == (assign.Value as Ident).Value)
                        {
                            isUseless = true;
                        }
                        else
                        {
                            result += (assign.Value as ObjectIdent).Name + "." + (assign.Value as ObjectIdent).Field;
                        }
                    }
                    else if (assign.Value is ObjectBlock)
                    {
                        ObjectBlock objectBlock = (assign.Value as ObjectBlock);
                        result += "{ ";

                        foreach (var arg in objectBlock.Fields)
                        {
                            result += arg.Key + " = " + arg.Value.Value.ToString() + ", ";
                        }
                        int indexToRemove = result.LastIndexOf(", ");
                        result = result.Substring(0, indexToRemove);
                        result += " }";
                    }
                    else if (assign.Value is MathExpr)
                    {
                        MathExpr expr = (assign.Value as MathExpr);
                        if (expr.Operation == MathOperation.DECREMENT)
                        {
                            result = result.Replace(" = ", "--");
                        }
                        else if (expr.Operation == MathOperation.INCREMENT)
                        {
                            result = result.Replace(" = ", "++");
                        }
                    }
                }
                else if (statement is Call)
                {
                    Call call = (statement as Call);
                    result += call.Ident + "(";

                    foreach (var arg in call.Args)
                    {
                        if (arg is IntLiteral)
                        {
                            result += "\"" + (arg as IntLiteral).Value.ToString() + "\"" + ", ";
                        }
                        else if (arg is StringLiteral)
                        {
                            result += "\"" + (arg as StringLiteral).Value + "\"" + ", ";
                        }
                        else if (arg is Ident)
                        {
                            result += (arg as Ident).Value + ", ";
                        }
                    }
                    int indexToRemove = result.LastIndexOf(", ");
                    result = result.Substring(0, indexToRemove);
                    result += ")";
                }

                item.Items.Add(result);
            }
            if (!isUseless || !isConditionBlock)
            {
                masterItem.Items.Add(item);
            }
            else if (isUseless && isConditionBlock)
            {
                for (int i = 0; i < item.Items.Count; ++i)
                {
                    TreeViewItem itm = (item.Items[i] as TreeViewItem);
                    item.Items.RemoveAt(i);
                    masterItem.Items.Add(itm);
                }
            }
        }
    }
}
