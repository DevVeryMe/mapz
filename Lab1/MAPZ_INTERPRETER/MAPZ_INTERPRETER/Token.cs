﻿using System.Collections.Generic;

namespace MAPZ_INTERPRETER
{
    public enum TokenType
    {
        IDENT,
        SEMICOLON,  // ;
        DOT,        // .
        COMMA,      // ,
        EOF,
        LEFT_PARAN,  // (
        RIGHT_PARAN, // )
        LEFT_BRACE,  // {
        RIGHT_BRACE, // }


        // KEY_WORDS
        VAR_KEYWORD,
        OBJECT_KEYWORD,

        // LITERALS
        INT_LIT,
        STRING_LIT,

        // OPERATIONS
        ASSIGN_OP,   // =
        DIVISION_OP, // /
        MULTIPLY_OP, // *
        SUM_OP,      // +
        SUBSTACT_OP, // -

        // OPERATORS
        EQUALS,         // ==
        NOT_EQUALS,     // !=
        MORE_THAN,      // >
        LESS_THAN,      // <
        MORE_OR_EQUAL,  // >=
        LESS_OR_EQUAL,  // <=
        INCREMENT,      // ++
        DECREMENT,      // --
        NOT,            // -

        // EMBEDDED_FUNCTIONS
        PRINT,
        CLICK_ELEMENT,
        FILL_TEXT_FIELD,
        LOAD_URL,

        // BLOCKS
        WHILE,
        FOR,
        IF,
        ELSE_IF,
        ELSE,

        // IGNORABLE
        WHITE_SPACE,
        NEW_LINE,
        COMMENT,
        UNDEFINED
    }
    public class Token
    {
        public string Data { get; set; }
        public TokenType Type { get; set; }

        public Token() { }
        public Token(string data, TokenType type)
        {
            Data = data;
            Type = type;
        }
    }
    public class TokenList
    {
        private List<Token> Tokens;
        private int position;

        public TokenList(List<Token> tokens)
        {
            position = 0;
            Tokens = tokens;
        }

        public Token GetToken()
        {
            Token ret = Tokens[position];
            position++;
            return ret;
        }

        public Token Peek()
        {
            return Tokens[position];
        }

        public void MoveToNext()
        {
            ++position;
        }
        public void MoveToPrev()
        {
            --position;
        }
    }

    public enum VarType
    {
        Integer,
        String
    }
    public class Variable
    {
        public string Name { get; set; }
        public object Value { get; set; }
        public VarType Type { get; set; }

        public Variable()
        {
            Name = string.Empty;
            Value = null;
        }
        public Variable(string name, object value, VarType type)
        {
            Name = name;
            Value = value;
            Type = type;
        }
    }

    public class CustomObject
    {
        public Dictionary<string, Variable> _fields;
        public string Name { get; set; }

        public CustomObject()
        {
            _fields = new Dictionary<string, Variable>();
        }
    }
}
