﻿using System;

namespace MAPZ_INTERPRETER
{
    class SyntaxException : Exception
    {
        protected readonly string _info;

        public SyntaxException(string info)
        {
            _info = info;
        }

        public string Info
        {
            get
            {
                return _info;
            }
        }
    }
}
