﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Naruto_MAPZ.Memento
{
    //[1]MEMENTO
    public class GameHistory //[1] MEMENTO CARETAKER
    {
        public Stack<UserMemento> History { get; private set; }
        public GameHistory()
        {
            History = new Stack<UserMemento>();
        }
    }
}
