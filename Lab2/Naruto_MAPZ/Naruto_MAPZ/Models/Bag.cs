﻿using Naruto_MAPZ.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Naruto_MAPZ.Models
{
    public class Bag
    {
        private Dictionary<string, IBagItem> _bagItems;
        private int _itemsMaxCount;
        private int _itemsCount;
        private bool _isFull;

        public Dictionary<string, IBagItem> Items
        {
            get
            {
                return _bagItems;
            }
        }

        public Bag()
        {

        }

        public static Bag Instance() //[1]
        {
            return lazy.Value;
        }
        public int ItemsMaxCount
        {
            get
            {
                return _itemsMaxCount;
            }
        }
        public int ItemsCount
        {
            get
            {
                return _itemsCount;
            }
        }
        public bool IsFull
        {
            get
            {
                return _isFull;
            }
        }
        public int SellItem(string name)
        {
            int compensation;
            compensation = _bagItems[name].SellPrice;
            _bagItems.Remove(name);

            return compensation;
        }

        public bool UpgradeItem(string name)
        {
            if (User.Instance().Cash >= _bagItems[name].UpgradePrice)
            {
                //TODO: Buy event must be here
                _bagItems[name].Upgrade();
                return true;
            }

            return false;
        }
    }
}
