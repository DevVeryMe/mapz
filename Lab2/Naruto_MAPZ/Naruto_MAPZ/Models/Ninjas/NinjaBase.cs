﻿using Naruto_MAPZ.Models.Ninjas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Naruto_MAPZ.Models.Ninjas
{
    //[1]FLYWEIGHT
    public class NinjaBase //[1]FLYWEIGHT FACTORY
    {
        private Dictionary<string, NinjaPrototype> _ninjas;


        public Dictionary<string, NinjaPrototype> Ninjas
        {
            get
            {
                return _ninjas;
            }
        }

        public void AddNinja(NinjaPrototype ninja)
        {
            _ninjas.Add(ninja.Name, ninja);
        }

        public void RemoveNinja(string name)
        {
            _ninjas.Remove(name);
        }

        public NinjaPrototype GetNinja(string name)
        {
            if (_ninjas.ContainsKey(name))
            {
                return _ninjas[name];
            }
            else
            {
                throw new Exception("Not existing ninja.");
            }
        }
    }
}
