﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Naruto_MAPZ.Models.Ninjas;

namespace Naruto_MAPZ.Models
{
    //[1] DECORATOR
    public abstract class Mission //[1] DECORATOR COMPONENT
    {
        protected MissionCustomer _customer;
        protected MissionRank _rank;
        protected int _compensation;
        protected int _completeChance;
        protected string _description;


        public MissionCustomer Customer
        {
            get
            {
                return _customer;
            }
        }
        public MissionRank Rank
        {
            get
            {
                return _rank;
            }
        }
        public int Compensation
        {
            get
            {
                return _compensation;
            }
        }
        public string Description
        {
            get
            {
                return _description;
            }
        }

        public Mission(MissionCustomer customer, MissionRank rank, string description)
        {
            _customer = customer;
            _rank = rank;
            _description = description;

            switch(rank)
            {
                case MissionRank.D:
                    _compensation = 100;
                    break;
                case MissionRank.C:
                    _compensation = 200;
                    break;
                case MissionRank.B:
                    _compensation = 400;
                    break;
                case MissionRank.A:
                    _compensation = 800;
                    break;
                case MissionRank.S:
                    _compensation = 1600;
                    break;
                case MissionRank.SS:
                    _compensation = 3200;
                    break;
                case MissionRank.SSS:
                    _compensation = 6400;
                    break;
                default:
                    break;
            }
        }

        public abstract int Complete(NinjaPrototype capitan, params NinjaPrototype[] ninjas);
    }

    public class OfficialMission : Mission //[1] DECORATOR CONCRETE COMPONENT
    {
        public OfficialMission(MissionCustomer customer, MissionRank rank, string description) : base(customer, rank, description)
        {
            _description += "\nStatus: Official";
        }

        public override int Complete(NinjaPrototype capitan, params NinjaPrototype[] ninjas)
        {
            throw new NotImplementedException();
        }
    }
    public class SecretMission : Mission //[1] DECORATOR CONCRETE COMPONENT
    {
        public SecretMission(MissionCustomer customer, MissionRank rank, string description) : base(customer, rank, description)
        {
            _description += "\nStatus: Secret";
            _compensation *= 2;
        }

        public override int Complete(NinjaPrototype capitan, params NinjaPrototype[] ninjas)
        {
            throw new NotImplementedException();
        }
    }

    public enum MissionRank
    {
        D,
        C,
        B,
        A,
        S,
        SS,
        SSS
    }
}
