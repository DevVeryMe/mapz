﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Naruto_MAPZ.Models.Villages;

namespace Naruto_MAPZ.VillageFactory
{
    class KirigakureVillageFactory : AbstractVillageFactory
    {
        public override AbstractVillage CreateVillage()
        {
            return new KirigakureVillage("Kirigakure");
        }
    }
}
