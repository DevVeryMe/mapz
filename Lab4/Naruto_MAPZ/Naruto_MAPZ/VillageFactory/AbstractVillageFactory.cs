﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Naruto_MAPZ.Models;
using Naruto_MAPZ.Models.Villages;

namespace Naruto_MAPZ.VillageFactory
{
    public abstract class AbstractVillageFactory
    {
        public abstract AbstractVillage CreateVillage();
    }
}
