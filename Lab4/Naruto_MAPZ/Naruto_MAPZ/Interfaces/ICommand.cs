﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Naruto_MAPZ.Interfaces
{
    //[1]COMMAND
    public interface ICommand 
    {
        void Execute();
        void Undo();
    }
}
