﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Naruto_MAPZ.Models.Enemies
{
    //[1]STRATEGY
    public interface IHitable //[1] ISTRATEGY
    {
        int Hit(int baseDamage, int level);
    }

    public class DamagingHit : IHitable //[1] CONCRETE STRATEGY
    {
        public int Hit(int baseDamage, int level)
        {
            int damage = (-1) * (baseDamage + (int)(level * 0.5));
            return damage;
        }
    }

    public class HealingHit : IHitable //[1] CONCRETE STRATEGY
    {
        public int Hit(int baseDamage, int level)
        {
            int damage = baseDamage + (int)(level * 0.5);
            return damage;
        }
    }
}
