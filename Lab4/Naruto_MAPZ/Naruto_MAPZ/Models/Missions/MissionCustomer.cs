﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Naruto_MAPZ.Models.Observer;
using Naruto_MAPZ.Models.Missions;
using System.Threading;

namespace Naruto_MAPZ.Models
{
    //[1] OBSERVER
    public class MissionCustomer : IObserver //[1] OBSERVER OBSERVER
    {
        private Mission _currentMission;
        private CancellationToken _cancellationToken;

        public MissionCustomer()
        {
            OrderMission();
        }

        private async Task StartOrdering()
        {
            while (true)
            {
                if (_currentMission == null)
                {
                    await Task.Run(() => OrderMission());
                }
                await Task.Delay(new TimeSpan(0, 20, 0), _cancellationToken);
                if (_cancellationToken.IsCancellationRequested)
                {
                    break;
                }
            }
        }

        private MissionRank GenerateRank()
        {
            MissionRank rank;
            Random random = new Random();

            if (User.Instance().Level < 7)
            {
                int chance = random.Next(0, 100);
                if (chance > 50)
                {
                    rank = MissionRank.D;
                }
                else if (chance > 20 && chance <= 50)
                {
                    rank = MissionRank.C;
                }
                else
                {
                    rank = MissionRank.B;
                }
            }
            else if (User.Instance().Level < 15)
            {
                int chance = random.Next(0, 100);
                if (chance > 50)
                {
                    rank = MissionRank.C;
                }
                else if (chance > 20 && chance <= 50)
                {
                    rank = MissionRank.B;
                }
                else
                {
                    rank = MissionRank.A;
                }
            }
            else
            {
                int chance = random.Next(0, 100);
                if (chance > 50)
                {
                    rank = MissionRank.A;
                }
                else if (chance > 20 && chance <= 50)
                {
                    rank = MissionRank.S;
                }
                else if (chance > 10 && chance <= 20)
                {
                    rank = MissionRank.SS;
                }
                else
                {
                    rank = MissionRank.SSS;
                }
            }

            return rank;
        }

        public void Update(Mission mission) //[1] OBSERVER UPDATE
        {
            if (mission.GetHashCode() == _currentMission.GetHashCode())
            {
                OrderMission();
            }
        }

        public void OrderMission()
        {
            MissionRank rank = GenerateRank();
            MissionDecorator mission;

            Random random = new Random();
            if (random.Next(0, 100) > 90)
            {
                mission = new MissionDecorator(this, rank, "xxx", new SecretMission(this, rank, "description"));
            }
            else
            {
                mission = new MissionDecorator(this, rank, "xxx", new OfficialMission(this, rank, "description"));
            }

            if (mission.Rank >= MissionRank.S)
            {
                if (random.Next(0, 100) > 80)
                {
                    if (random.Next(0, 100) > 50)
                    {
                        MissionAgainstNukenin missionAgainstNukenin = new MissionAgainstNukenin(this, rank, "xxx", mission);
                        mission = missionAgainstNukenin;
                    }
                    else
                    {
                        MissionAgainstBiju missionAgainstBiju = new MissionAgainstBiju(this, rank, "xxx", mission);
                        mission = missionAgainstBiju;
                    }
                }
            }

            _currentMission = mission;
            User.Instance().NewMissionAvailable(mission);
        }
    }
}
