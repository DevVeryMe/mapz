﻿using Naruto_MAPZ.Models.Ninjas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Naruto_MAPZ.Models.Missions
{
    //[1]DECORATOR
    public class MissionDecorator : Mission //[1]DECORATOR
    {
        protected Mission _mission;
        public MissionDecorator(MissionCustomer customer, MissionRank rank, string description, Mission mission) : base(customer, rank, description)
        {
            this._mission = mission;
        }

        public override int Complete(NinjaPrototype capitan, params NinjaPrototype[] ninjas)
        {
            throw new NotImplementedException();
        }
    }

    public class MissionAgainstNukenin : MissionDecorator //[1]CONCRETE DECORATOR
    {
        public MissionAgainstNukenin(MissionCustomer customer, MissionRank rank, string description, Mission mission) : base(customer, rank, description, mission)
        {
            _description += "Fight with nukenin.";
            _compensation += 1600;
        }
        public override int Complete(NinjaPrototype capitan, params NinjaPrototype[] ninjas)
        {
            throw new NotImplementedException();
        }
    }

    public class MissionAgainstBiju : MissionDecorator //[1]CONCRETE DECORATOR
    {
        public MissionAgainstBiju(MissionCustomer customer, MissionRank rank, string description, Mission mission) : base(customer, rank, description, mission)
        {
            _description += "Fight with biju.";
            _compensation += 3200;
        }
        public override int Complete(NinjaPrototype capitan, params NinjaPrototype[] ninjas)
        {
            throw new NotImplementedException();
        }
    }
}
