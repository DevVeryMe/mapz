﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Naruto_MAPZ.Models.Villages
{
    class KirigakureVillage : AbstractVillage
    {
        public KirigakureVillage(string style) : base(style)
        {
            _style += "Water Style";
        }
    }
}
