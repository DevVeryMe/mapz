﻿using Naruto_MAPZ.Models;
using Naruto_MAPZ.Models.Ninjas;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Naruto_MAPZ.Memento
{
    //[1]MEMENTO
    [Serializable]
    public class UserMemento //[1] MEMENTO
    {
        public int Cash { get; private set; }
        public int Level { get; private set; }
        public int Experience { get; set; }
        public NinjaBase NinjaBase { get; set; }
        public Bag Bag { get; set; }

        public UserMemento()
        {
            
        }

        public void Save(int cash, int level, int experience, NinjaBase ninjaBase, Bag bag)
        {
            this.Cash = cash;
            this.Level = level;
            this.Experience = experience;
            this.NinjaBase = ninjaBase;
            this.Bag = bag;

            BinaryFormatter formatter = new BinaryFormatter();

            using (FileStream file = new FileStream("LastSave.bin", FileMode.OpenOrCreate))
            {
                formatter.Serialize(file, this);
            }
        }

        public static UserMemento LoadLast()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            UserMemento saver = new UserMemento();

            using (FileStream file = new FileStream("LastSave.bin", FileMode.OpenOrCreate))
            {
                try
                {
                    saver = (UserMemento)formatter.Deserialize(file);
                }
                catch (ArgumentNullException ex)
                {
                    return null;
                }
            }

            return saver;
        }

    }
}
